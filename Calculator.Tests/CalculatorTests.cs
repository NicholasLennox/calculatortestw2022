using System;
using Xunit;

namespace Calculator.Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void Add_TwoValidNumbers_ShouldReturnSum()
        {
            // Arrange
            Calculator calculator = new Calculator();
            double lhs = 1;
            double rhs = 1;
            double expected = lhs + rhs;
            // Act
            double actual = calculator.Add(lhs, rhs);
            // Assert
            Assert.Equal(expected, actual); // ==
            Assert.True(expected.Equals(actual)); // .Equals
        }

        [Fact]
        public void Divide_ZeroDenominator_ShouldThrowDivideByZeroException()
        {
            // Arrange
            Calculator calculator = new Calculator();
            double lhs = 1;
            double rhs = 0;
            string expected = "Cannot divide by zero";
            // Act
            var exception = Assert.Throws<DivideByZeroException>(() => calculator.Divide(lhs, rhs));
            string actual = exception.Message;
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
