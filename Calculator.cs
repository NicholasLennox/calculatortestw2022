﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Calculator
    {
        public double Add(double lhs, double rhs)
        {
            return lhs + rhs;
        }

        public double Divide(double lhs, double rhs)
        {
            if (rhs == 0)
                throw new DivideByZeroException("Cannot divide by zero");
            return lhs / rhs;
        }
    }
}
